import {mapState} from 'vuex'
import api from '../../api'

export default {
	name: 'weather',
	data: function(){
  	return{
		prev: [],
		prev_now: [],
		local: '',
		info_local: [],
	    favorites: []
	}	
  },
 	methods: {
  		goToFavorite(favorite){
	    	this.local=favorite 
	    	this.getDataForecast()
	    },
	    addFavorite(){
	    	var self = this
	    	for(var i = 0; i < this.favorites.length; i++){
	    		if(this.info_local[0]['name'] === this.favorites[i]){
	    			return
	    		}
			}
	    	this.favorites.push(this.info_local[0]['name'])
	    },
	    clear(){
	    	this.local = ''
	    	this.prev = []
	    	this.prev_now = []
	    	this.info_local = []
	    },
	   	getDataForecast(){
	   		this.prev = []
	    	this.info_local = []
	    	this.prev_now = []
	    	var self = this
	    	// Tempo agora!
	   		api.now({q: this.local.trim()}, function(data, err){
	    		if (err) {
					console.error(err)
				}
				self.prev_now.push({
					weather: data['weather'][0]['description'],
					humidity: data['main']['humidity'],
					temp: String(parseFloat(data['main']['temp']-273.15).toFixed(2)) + "C°",
					icon: "http://openweathermap.org/img/w/"+data['weather'][0]['icon']+".png"
				})
			})
			// Previsão de 5 dias
	    	api.forecast({q: this.local.trim()}, function(data, err) {
				if (err) {
					console.error(err)
				}
				// Realizei essas transformações pois ficou mais fácil de trabalhar com a lista de data.
				var data = JSON.stringify(data)
				var parsed = JSON.parse(data)
				var arr = []
				for(var x in parsed){
			  		arr.push(parsed[x]);
				}
				// Pode acontecer de o indice dos objetos de previsão venham trocados, por isso verifico
				// em qual posição que o array de objetos estará.
				if((typeof(arr[3])) !== 'object' ){
					var indice = 4
					var ind_local = 0
				}
				else{
					var indice = 3
					var ind_local = 4
				}
				self.info_local.push({
					name: arr[ind_local]['name'],
					country: arr[ind_local]['country']
				})
				// As informações de previsão chegam em um array de 40 posições, com atualizações a cada 3 horas,
				// para separar por dia, o i do for tem que caminhar de 8 em 8, pois 3x8=24h.
				for(var i=0;  i <=40 ; i+=8){
					if(self.prev.length == 5){
						break
					}
					// Como as temperaturas mínimas e máximas são por cada hora, esse próximo bloco percorre
					// cada dia e compara a temperatura mínima e máxima de cada hora, para dar um valor correto
					// para temperatura mínima e máxima do DIA.
					var max_temp = 0
					var min_temp = 1000
					var max_hour, min_hour = ''
					for(var y = i; y < i+7; y++){
						if (max_temp < parseFloat(arr[indice][y]['main']['temp_max'])){
							max_temp = parseFloat(arr[indice][y]['main']['temp_max'])
							max_hour = arr[indice][y]['dt_txt'].split(" ")[1]
						}
						if (min_temp > parseFloat(arr[indice][y]['main']['temp_min'])){
							min_temp = parseFloat(arr[indice][y]['main']['temp_min'])
							min_hour = arr[indice][y]['dt_txt'].split(" ")[1]
						}
					}
					// Adicionando ao array prev os valores de cada dia
					self.prev.push({
						day: arr[indice][i]['dt_txt'].split(" ")[0], 
						weather: arr[indice][i]['weather'][0]['description'],
						humidity: arr[indice][i]['main']['humidity'],
						temp_max: String((max_temp-273.15).toFixed(2)) + "C° " + "at " + max_hour,
						temp_min: String((min_temp-273.15).toFixed(2)) + "C° " + "at " + min_hour,
						icon: "http://openweathermap.org/img/w/"+arr[indice][i]['weather'][0]['icon']+".png"
					})
				}
			})
	    }
	}
}
